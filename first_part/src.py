""" (I repeat myself I will not correct your test if you don't change visibility of the project from public to private, so please start with this)
In the function exercise_one on first_part.src module:
print every number between 1 and 100 as follows:

For every multiple of 3 print "Three".
For every multiple of 5 print "Five".
And for every multiple of both 3 and 5 print "ThreeFive"  """

def exercise_one():
    for i in range(1, 101):
        if i % 3 == 0 and i % 5 == 0:
            print("ThreeFive")
        elif i % 3 == 0:
            print("Three")
        elif i % 5 == 0:
            print("Five")
        else:
            print(i)


exercise_one()


""" Determine whether a positive integer number is colorful or not.
263 is a colorful number because [2, 6, 3, 2x6, 6x3, 2x6x3] are all different; whereas 236 is not colorful, because [2, 3, 6, 2x3, 3x6, 2x3x6] have 6 twice."""

def is_colorful(number):
    digits = [int(digit) for digit in str(number)]
    products = set()

    for i in range(len(digits)):
        product = 1
        for j in range(i, len(digits)):
            product *= digits[j]
            if product in products:
                return False
            products.add(product)

    return True
print(is_colorful(263))  # Output: True
print(is_colorful(236))  # Output: False
print(is_colorful(2532))  # Output: False


""" Write a function calculate that takes a list of strings a returns the sum of the list items that represents an integer (skipping the other items).
Examples """

def calculate(lst):
    total = 0

    if isinstance(lst, list):
        for item in lst:
            if isinstance(item, str) and item.lstrip('-').isdigit():
                total += int(item)

    return total if total != 0 else False


print(calculate(['4', '3', '-2']))  # Output: 5
print(calculate([453]))  # Output: False
print(calculate(['nothing', 3, '8', 2, '1']))  # Output: 9
print(calculate('54'))  # Output: False

""" Write a function that will find all the anagrams of a word from a list. You will be given two inputs a word and an array with words. You should return an array of all the anagrams or an empty array if there are none. """


def anagrams(word, words):
    sorted_word = sorted(word)
    return [w for w in words if sorted(w) == sorted_word]


print(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']))  # Output: ['aabb', 'bbaa']
print(anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']))  # Output: ['carer', 'racer']
print(anagrams('laser', ['lazing', 'lazy', 'lacer']))  # Output: []


