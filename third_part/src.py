""" Create a function http_request that sends a post request to this url https://httpbin.org/anything with this parameters:

isadmin=1
and return the response body.
Now, send the same request this time by changing the user-agent """
import requests

def http_request():
    url = "https://httpbin.org/anything"
    params = {"isadmin": 1}
    headers = {"User-Agent": "Custom-Agent"}

    response = requests.post(url, params=params, headers=headers)

    return response.text

# Send the request with default user-agent
response_default_ua = http_request()
print("Response with default User-Agent:")
print(response_default_ua)

# Send the request with custom user-agent
response_custom_ua = http_request()
print("\nResponse with custom User-Agent:")
print(response_custom_ua)


""" Exercise 2  """
import json  # Import the json module for working with JSON data
import gzip  # Import the gzip module for reading compressed files
import csv   # Import the csv module for working with CSV files

class ProductManager:

    def __init__(self):
        self.products = []  # Initialize an empty list to hold product data

    def load_data(self):
        try:
            # Load data from data.json.gz
            with gzip.open('third_part/data/data.json.gz', 'rb') as file:
                self.products = json.load(file)  # Load JSON data from the compressed file
        except Exception as e:
            # Log an error if loading fails
            print(f"Error loading data: {e}")

    def process_products(self):
        available_products = []  # Initialize a list to hold available products

        for product in self.products:
            product_name = product.get('product_name', '')  # Get product name or empty string if not available
            product_id = product.get('product_id', '')  # Get product id or empty string if not available
            product_price = round(float(product.get('product_price', 0)), 1)  # Get and round product price

            if product_name and product_price > 0:  # Check if product is available
                truncated_name = product_name[:30]  # Truncate product name to 30 characters
                available_products.append({'name': truncated_name, 'price': product_price})  # Add to available products
            else:
                # Log unavailable product
                print(f"Product unavailable: ID {product_id}, Name {product_name}")

        return available_products  # Return the list of available products

    def save_to_csv(self, data):
        try:
            with open('available_products.csv', mode='w', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(['Product Name', 'Product Price'])  # Write header row to CSV file
                for item in data:
                    writer.writerow([item['name'], item['price']])  # Write product data to CSV
        except Exception as e:
            # Log an error if saving fails
            print(f"Error saving to CSV: {e}")

    def print_available_products(self):
        for product in self.process_products():
            print(f'You can buy {product["name"]} at our store at {product["price"]}')

# Usage example
manager = ProductManager()  # Create an instance of ProductManager
manager.load_data()  # Load product data
manager.print_available_products()  # Print available products

"""Exercise 3 """
#Scrapy installed
pip install scrapy

#Next, create a new Scrapy project. Navigate to the desired location for your project and run:

scrapy startproject woolworths_spider

#Open the items.py file within the woolworths_spider.
import scrapy

class WoolworthsSpiderItem(scrapy.Item):
    product_name = scrapy.Field()
    breadcrumb = scrapy.Field()

#Create a new Python file
import scrapy
from woolworths_spider.items import WoolworthsSpiderItem

class WoolworthsSpider(scrapy.Spider):
    name = 'woolworths'
    start_urls = ['https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas']

    def parse(self, response):
        breadcrumb = response.xpath('//*[@id="search-content"]/div/shared-breadcrumb/nav/ul/li/span/text()').getall()
        product_names = response.xpath("//div/a[@class='product-title-link ng-star-inserted']/text()").getall()

        for product_name in product_names:
            item = WoolworthsSpiderItem()
            item['product_name'] = product_name.strip()
            item['breadcrumb'] = breadcrumb[1:]
            yield item

#Open the settings.py file and make sure FEED_FORMAT is set to 'csv'
FEED_FORMAT = 'csv'
FEED_URI = 'output.csv'
#Run the Spider:
scrapy crawl woolworths
 # another option
 
 scrapy crawl woolworths -o output.csv

# This will run the spider and save the results in a CSV file 
